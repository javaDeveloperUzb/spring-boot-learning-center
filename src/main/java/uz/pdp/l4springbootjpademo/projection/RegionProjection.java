package uz.pdp.l4springbootjpademo.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.l4springbootjpademo.entity.District;
import uz.pdp.l4springbootjpademo.entity.Region;
import uz.pdp.l4springbootjpademo.payload.DistrictDto;

import java.util.List;

@Projection(name = "regionProjection",types = Region.class)
public interface RegionProjection {
    Integer getId();
    String getName();

    @Value("#{@districtRepository.allByRegionId(target.id)}")
    List<District> getAllByRegionId();
}

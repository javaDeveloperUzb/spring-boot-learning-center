package uz.pdp.l4springbootjpademo.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.l4springbootjpademo.entity.Client;
import uz.pdp.l4springbootjpademo.payload.ApiResponse;
import uz.pdp.l4springbootjpademo.payload.ClientDto;
import uz.pdp.l4springbootjpademo.repository.ClientRepository;
import uz.pdp.l4springbootjpademo.utills.CommonUtills;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {
    private final
    ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public ApiResponse saveOrEdit(ClientDto dto) {
        try {
            Client client = new Client();
            if (dto.getId() != null) {
                client = clientRepository.findById(dto.getId())
                        .orElseThrow(() -> new IllegalArgumentException("Client with this"));
            }
            client.setName(dto.getName());
            client.setPhoneNumber(dto.getPhoneNumber());
            clientRepository.save(client);
            return new ApiResponse(dto.getId() != null ? "Edited" : "Saved", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse getAllClients(Integer page, Integer size) {
        Page<Client> clientPage = clientRepository.findAll(CommonUtills.simplePageable(page, size));
        return new ApiResponse("Success",
                true,
                clientPage.getTotalElements(),
                clientPage.getTotalPages(),
                clientPage.getContent().stream().map(this::getClientDtoFromClient).collect(Collectors.toList()));
    }
    public ClientDto getClientDtoFromClient(Client client) {
        ClientDto dto = new ClientDto();
        dto.setId(client.getId());
        dto.setName(client.getName());
        dto.setPhoneNumber(client.getPhoneNumber());
        return dto;
    }

    public ApiResponse getOneClientById(Integer id) {
        try {
            Client client = clientRepository.findById(id).orElseThrow(() ->
                    new IllegalArgumentException("Client with this id not found"));
            return new ApiResponse("Client found",
                    true,
                    getClientDtoFromClient(client));
        } catch (Exception e) {
            return new ApiResponse("Client with this id not found",false);
        }
    }

    public ApiResponse searchByName(String name) {
        try {
            List<Client> clients = clientRepository.searchByName(name);
            if (clients.isEmpty()) {
                return new ApiResponse("Client not found",false);
            } else {
                return new ApiResponse("Client found",
                        true,clients.stream().map(this::getClientDtoFromClient).collect(Collectors.toList()));
            }
        } catch (Exception e) {
            return new ApiResponse("Client with this id not found",false);
        }
    }

    public ApiResponse removeById(Integer id){
        try {
            clientRepository.deleteById(id);
            return new ApiResponse("Deleted",true);
        }catch (Exception e) {
            return new ApiResponse("Erron in deleting",false);
        }

    }
}

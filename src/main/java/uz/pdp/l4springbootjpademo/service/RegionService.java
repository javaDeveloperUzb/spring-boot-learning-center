package uz.pdp.l4springbootjpademo.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.l4springbootjpademo.entity.Region;
import uz.pdp.l4springbootjpademo.payload.ApiResponse;
import uz.pdp.l4springbootjpademo.payload.RegionDto;
import uz.pdp.l4springbootjpademo.repository.RegionRepository;
import uz.pdp.l4springbootjpademo.utills.CommonUtills;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RegionService {
    private final
    RegionRepository regionRepository;

    public RegionService(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

//    public ApiResponse saveOrEdit(RegionDto dto) {
//        try {
//            Region region = new Region();
//            if (dto.getId() != null) {
//                region = regionRepository.findById(dto.getId())
//                        .orElseThrow(() ->  new IllegalArgumentException("Region with this id not found"));
//            }
//            region.setName(dto.getName());
//            regionRepository.save(region);
//            return new ApiResponse(dto.getId() != null ? "Edited" : "Saved", true);
//        } catch (Exception e) {
//            return new ApiResponse("Error", false);
//        }
//    }
//
//    public ApiResponse getAllRegions(Integer page, Integer size) {
//        Page<Region> regionPage = regionRepository.findAll(CommonUtills.simplePageable(page, size));
//        return new ApiResponse("Success",
//                true,
//                regionPage.getTotalElements(),
//                regionPage.getTotalPages(),
//                regionPage.getContent().stream().map(this::getRegionDtoFromRegion).collect(Collectors.toList()));
//    }
//    public RegionDto getRegionDtoFromRegion(Region region) {
//        RegionDto dto = new RegionDto();
//        dto.setId(region.getId());
//        dto.setName(region.getName());
//        return dto;
//    }
//
//    public ApiResponse getOneRegionById(Integer id) {
//        try {
//            Region region = regionRepository.findById(id).orElseThrow(() ->
//                    new IllegalArgumentException("Client with this id not found"));
//            return new ApiResponse("Client found",
//                    true,
//                    getRegionDtoFromRegion(region));
//        } catch (Exception e) {
//            return new ApiResponse("Client with this id not found",false);
//        }
//    }
//
//    public ApiResponse searchByName(String name) {
//        try {
//            List<Region> regions = regionRepository.searchByName(name);
//            if (regions.isEmpty()) {
//                return new ApiResponse("Region not found",false);
//            } else {
//                return new ApiResponse("Region found",
//                        true,regions.stream().map(this::getRegionDtoFromRegion).collect(Collectors.toList()));
//            }
//        } catch (Exception e) {
//            return new ApiResponse("Region with this id not found",false);
//        }
//    }
//
//    public ApiResponse removeById(Integer id){
//        try {
//            regionRepository.deleteById(id);
//            return new ApiResponse("Deleted",true);
//        }catch (Exception e) {
//            return new ApiResponse("Erron in deleting",false);
//        }
//
//    }
}

package uz.pdp.l4springbootjpademo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.pdp.l4springbootjpademo.entity.District;
import uz.pdp.l4springbootjpademo.payload.ApiResponse;
import uz.pdp.l4springbootjpademo.payload.DistrictDto;
import uz.pdp.l4springbootjpademo.repository.DistrictRepository;
import uz.pdp.l4springbootjpademo.repository.RegionRepository;
import uz.pdp.l4springbootjpademo.utills.CommonUtills;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DistrictService {
    private final
    DistrictRepository districtRepository;

    private final
    RegionRepository regionRepository;

    public DistrictService(DistrictRepository districtRepository, RegionRepository regionRepository) {
        this.districtRepository = districtRepository;
        this.regionRepository = regionRepository;
    }

    public ApiResponse saveOrEdit(DistrictDto dto) {
        try {
            District district = new District();
            if (dto.getId() != null) {
                district = districtRepository.findById(dto.getId())
                        .orElseThrow(() -> new IllegalStateException("District with this"));
            }
            if (dto.getRegionId() != null) {
                district.setRegion(regionRepository.findById(dto.getRegionId()).orElseThrow(() -> new IllegalStateException("Region not found")));
             }
                district.setName(dto.getName());
            districtRepository.save(district);
            return new ApiResponse(dto.getId() != null ? "Edited" : "Saved", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse getAllDistricts(Integer page, Integer size) {
        Page<District> districtPage = districtRepository.findAll(CommonUtills.simplePageable(page, size));
        return new ApiResponse("Succes",
                true,
                districtPage.getTotalElements(),
                districtPage.getTotalPages(),
                districtPage.getContent().stream().map(this::getDistrictDtoFromDistrict).collect(Collectors.toList()));
    }

    public DistrictDto getDistrictDtoFromDistrict(District district) {
        DistrictDto dto = new DistrictDto();
        dto.setId(district.getId());
        dto.setName(district.getName());
        return dto;
    }

    public ApiResponse getOneDistrictById(Integer id) {
        try {
            District district = districtRepository.findById(id).orElseThrow(() ->
                    new IllegalArgumentException("District with this id not found"));
            return new ApiResponse("District found",
                    true,
                    getDistrictDtoFromDistrict(district));
        } catch (Exception e) {
            return new ApiResponse("District with this id not found", false);
        }
    }

    public ApiResponse searchByName(String name) {
        try {
            List<District> districts = districtRepository.searchByName(name);
            if (districts.isEmpty()) {
                return new ApiResponse("Client not found", false);
            } else {
                return new ApiResponse("Client found",
                        true, districts.stream().map(this::getDistrictDtoFromDistrict).collect(Collectors.toList()));
            }
        } catch (Exception e) {
            return new ApiResponse("Client with this id not found", false);
        }
    }

    public ApiResponse removeById(Integer id) {
        try {
            districtRepository.deleteById(id);
            return new ApiResponse("Deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Erron in deleting", false);
        }

    }
}

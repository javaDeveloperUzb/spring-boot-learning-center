package uz.pdp.l4springbootjpademo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class RoadMap extends AbsNameEntity {
    @ManyToMany
    @JoinTable(name = "yonalish_oquv_markaz",joinColumns = {@JoinColumn(name = "yonalish_id")},
            inverseJoinColumns = {@JoinColumn(name = "oquv_markaz_id")})
    private List<LearningCenter> learningCenterList;
}

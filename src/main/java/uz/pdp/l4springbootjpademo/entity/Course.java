package uz.pdp.l4springbootjpademo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Course extends AbsNameEntity {
    private Integer lessonAmount;
    private Integer lessonDurationInMinutes;

    @ManyToOne(fetch = FetchType.LAZY)
    private RoadMap roadMap;

    @ManyToOne(fetch = FetchType.LAZY)
    private LearningCenter learningCenter;

    private Double price;
}

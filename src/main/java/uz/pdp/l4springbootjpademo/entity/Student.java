package uz.pdp.l4springbootjpademo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Student extends AbsNameEntity{
    @Column(nullable = false)
    @Email
    private String email;

    @Column(length = 13, unique = true, nullable = false) //yok @Size
//    @Pattern(regexp = "^[+][9][9][8][0-9]{9}",message = "Phone number must be 13 digits");
    private String phoneNumber;

    @ManyToMany
    private List<Course> courses;
}

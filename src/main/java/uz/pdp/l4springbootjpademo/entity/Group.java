package uz.pdp.l4springbootjpademo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.l4springbootjpademo.entity.enums.StudyType;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "groups")
public class Group extends AbsNameEntity {
    @ManyToMany
    private List<Student> studentList;

    @ManyToMany
    private List<Mentor> mentorList;

    @Enumerated(value = EnumType.STRING)
    private StudyType studyType;

    @ManyToOne
    private Course course;

    private Timestamp startDate;
    private Time startTime;

    @ManyToOne
    private Room room;
    

}

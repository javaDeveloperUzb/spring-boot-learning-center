package uz.pdp.l4springbootjpademo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.l4springbootjpademo.entity.AbsNameEntity;
import uz.pdp.l4springbootjpademo.entity.Course;
import uz.pdp.l4springbootjpademo.entity.LearningCenter;
import uz.pdp.l4springbootjpademo.entity.enums.MentorStatus;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Mentor extends AbsNameEntity {
    private String phoneNumber;

    @ManyToMany
    private List<LearningCenter> learningCenterList;

    @ManyToMany
    private List<Course> courseList;

    @Enumerated(value = EnumType.STRING)
    private MentorStatus mentorStatus;
}

package uz.pdp.l4springbootjpademo.entity.enums;

public enum MentorStatus {
    MAIN,
    ASSISTANT
}

package uz.pdp.l4springbootjpademo.entity.enums;

public enum WeekDay {
    MONDAY,
    SUNDAY
}

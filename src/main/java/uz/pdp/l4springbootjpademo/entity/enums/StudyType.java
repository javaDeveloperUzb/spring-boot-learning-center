package uz.pdp.l4springbootjpademo.entity.enums;

public enum StudyType {
    ONLINE,
    OFFLINE,
    ONSITE
}

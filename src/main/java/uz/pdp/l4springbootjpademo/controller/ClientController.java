package uz.pdp.l4springbootjpademo.controller;

import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.l4springbootjpademo.payload.ApiResponse;
import uz.pdp.l4springbootjpademo.payload.ClientDto;
import uz.pdp.l4springbootjpademo.service.ClientService;
import uz.pdp.l4springbootjpademo.utills.ApplicationConstants;

@RestController
@RequestMapping("/api/client")
public class ClientController {
    private final
    ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping("/saveOrEdit")
    public HttpEntity<?> saveOrEdit(@RequestBody ClientDto dto) {
        ApiResponse apiResponse = clientService.saveOrEdit(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ?
                apiResponse.getMessage().equals("Saved") ? 201 : 202 : 409)
                .body(apiResponse);
    }

    @GetMapping("/getAllClients")
    public HttpEntity<?> getAllClients(@RequestParam(value = "page", defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                       @RequestParam(value = "size", defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(clientService.getAllClients(page, size));
    }

    @GetMapping("/byId/{id}")
    public HttpEntity<?> getOneCliendById(@PathVariable(value = "id") Integer id) {
        ApiResponse apiResponse = clientService.getOneClientById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/searchByName")
    public HttpEntity<?> searchByName(@Param(value = "name") String name) {
        ApiResponse apiResponse = clientService.searchByName(name);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> removeClientById(@PathVariable(value = "id") Integer id) {
        ApiResponse response = clientService.removeById(id);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

}

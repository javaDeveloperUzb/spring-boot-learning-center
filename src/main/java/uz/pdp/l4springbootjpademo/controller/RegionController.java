package uz.pdp.l4springbootjpademo.controller;

import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.l4springbootjpademo.payload.ApiResponse;
import uz.pdp.l4springbootjpademo.payload.RegionDto;
import uz.pdp.l4springbootjpademo.service.RegionService;
import uz.pdp.l4springbootjpademo.utills.ApplicationConstants;

@RestController
@RequestMapping("/api/region")
public class RegionController {
    private final
    RegionService regionService;

    public RegionController(RegionService regionService) {
        this.regionService = regionService;
    }

//    @PostMapping("/saveOrEdit")
//    public HttpEntity<?> saveOrEdit(@RequestBody RegionDto dto) {
//        ApiResponse apiResponse = regionService.saveOrEdit(dto);
//        return ResponseEntity.status(apiResponse.isSuccess() ?
//                apiResponse.getMessage().equals("Saved") ? 201 : 202 : 409).body(apiResponse);
//    }
//
//    @GetMapping("/getAllRegions")
//    public HttpEntity<?> getAllRegions(@RequestParam(value = "page",
//            defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
//                                         @RequestParam(value = "size",
//                                                 defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
//        return ResponseEntity.ok(regionService.getAllRegions(page, size));
//    }
//
//    @GetMapping("/byId/{id}")
//    public HttpEntity<?> getOneRegionById(@PathVariable(value = "id") Integer id) {
//        ApiResponse apiResponse = regionService.getOneRegionById(id);
//        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
//    }
//    @GetMapping("/searchByName")
//    public HttpEntity<?> searchByName(@Param(value = "name") String name) {
//        ApiResponse apiResponse = regionService.searchByName(name);
//        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
//    }
//    @DeleteMapping("/delete/{id}")
//    public HttpEntity<?> removeRegionById(@PathVariable(value = "id") Integer id) {
//        ApiResponse response = regionService.removeById(id);
//        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
//    }
}

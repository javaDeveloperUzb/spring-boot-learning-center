package uz.pdp.l4springbootjpademo.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.l4springbootjpademo.payload.ApiResponse;
import uz.pdp.l4springbootjpademo.payload.StudentDto;
import uz.pdp.l4springbootjpademo.service.StudentService;
import uz.pdp.l4springbootjpademo.utills.ApplicationConstants;

@RestController
@RequestMapping("/api/student")
public class StudentController {
    private final
    StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("/saveOrEdit")
    public HttpEntity<?> saveOrEditStudent(@RequestBody StudentDto dto) {
        ApiResponse apiResponse = studentService.saveOrEditStudent(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ?
                apiResponse.getMessage().equals("Saved") ? 201 : 202 : 409)
                .body(apiResponse);
    }

    @GetMapping("/getAllStudents")
    public HttpEntity<?> getAllStudents(@RequestParam(value = "page",
            defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                        @RequestParam(value = "size",
                                                defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(studentService.getAllStudents(page, size));
    }

    @GetMapping("/byId/{id}")
    public HttpEntity<?> getOneStudentById(@PathVariable(value = "id") Integer id) {
        ApiResponse response = studentService.getOneStudentById(id);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> removeStudentById(@PathVariable(value = "id") Integer id) {
        ApiResponse response = studentService.removeById(id);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

}

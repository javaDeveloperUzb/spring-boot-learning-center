package uz.pdp.l4springbootjpademo.controller;

import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.l4springbootjpademo.payload.ApiResponse;
import uz.pdp.l4springbootjpademo.payload.DistrictDto;
import uz.pdp.l4springbootjpademo.service.DistrictService;
import uz.pdp.l4springbootjpademo.utills.ApplicationConstants;

@RestController
@RequestMapping("/api/district")
public class DistrictController {
    private final
    DistrictService districtService;

    public DistrictController(DistrictService districtService) {
        this.districtService = districtService;
    }

    @PostMapping("/saveOrEdit")
    public HttpEntity<?> saveOrEdit(@RequestBody DistrictDto dto) {
        ApiResponse apiResponse = districtService.saveOrEdit(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ?
                apiResponse.getMessage().equals("Saved") ? 201 : 202 : 409).body(apiResponse);
    }

    @GetMapping("/getAllDistricts")
    public HttpEntity<?> getAllDistricts(@RequestParam(value = "page",
            defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                         @RequestParam(value = "size",
                                                 defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(districtService.getAllDistricts(page, size));
    }

    @GetMapping("/byId/{id}")
    public HttpEntity<?> getOneDistrictById(@PathVariable(value = "id") Integer id) {
        ApiResponse apiResponse = districtService.getOneDistrictById(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
    @GetMapping("/searchByName")
    public HttpEntity<?> searchByName(@Param(value = "name") String name) {
        ApiResponse apiResponse = districtService.searchByName(name);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> removeDistrictById(@PathVariable(value = "id") Integer id) {
        ApiResponse response = districtService.removeById(id);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }
}

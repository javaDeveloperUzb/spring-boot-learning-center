package uz.pdp.l4springbootjpademo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.l4springbootjpademo.entity.Client;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Integer> {
    List<Client> findAllByNameContainingIgnoreCase(String name);

    @Query(value = "select * from client where name like :name", nativeQuery = true)
    List<Client> searchByName(@Param(value = "name") String name);
}

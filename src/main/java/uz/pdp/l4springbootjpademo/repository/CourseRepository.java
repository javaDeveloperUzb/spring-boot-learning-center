package uz.pdp.l4springbootjpademo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.l4springbootjpademo.entity.Course;

public interface CourseRepository extends JpaRepository<Course,Integer> {

}

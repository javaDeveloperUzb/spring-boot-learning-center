package uz.pdp.l4springbootjpademo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.l4springbootjpademo.entity.District;
import uz.pdp.l4springbootjpademo.payload.DistrictDto;

import java.util.List;

public interface DistrictRepository extends JpaRepository<District, Integer> {
    List<District> findAllByNameContainingIgnoreCase(String name);

    @Query(value = "select * from district where name like :name", nativeQuery = true)
    List<District> searchByName(@Param(value = "name") String name);

    @Query(value = "select * from district where region_id =:regId", nativeQuery = true)
    List<District> allByRegionId(@Param(value = "regId") Integer regionId);

}

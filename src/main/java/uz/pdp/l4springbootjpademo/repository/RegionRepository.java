package uz.pdp.l4springbootjpademo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.l4springbootjpademo.entity.Region;
import uz.pdp.l4springbootjpademo.projection.RegionProjection;

import java.util.List;
@RepositoryRestResource(path = "region",collectionResourceRel = "list",excerptProjection = RegionProjection.class)
public interface RegionRepository extends JpaRepository<Region, Integer> {
    List<Region> findAllByNameContainingIgnoreCase(String name);

    @Query(value = "select * from region where name like :name", nativeQuery = true)
    List<Region> searchByName(@Param(value = "name") String name);
}

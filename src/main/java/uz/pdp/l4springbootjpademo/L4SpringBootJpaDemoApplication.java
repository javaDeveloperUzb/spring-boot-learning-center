package uz.pdp.l4springbootjpademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class L4SpringBootJpaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(L4SpringBootJpaDemoApplication.class, args);
    }

}

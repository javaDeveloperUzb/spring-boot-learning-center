package uz.pdp.l4springbootjpademo.payload;

import lombok.Data;

@Data
public class RegionDto {
    private Integer id;
    private String name;
}

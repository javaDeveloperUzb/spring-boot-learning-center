package uz.pdp.l4springbootjpademo.payload;

import lombok.Data;

@Data
public class CourseDto {
    private Integer id;
    private String name;
}

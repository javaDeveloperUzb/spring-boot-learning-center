package uz.pdp.l4springbootjpademo.payload;

import lombok.Data;

@Data
public class ClientDto {
    private Integer id;
    private String name;
    private String phoneNumber;
}

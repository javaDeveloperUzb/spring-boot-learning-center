package uz.pdp.l4springbootjpademo.payload;

import lombok.Data;

@Data
public class StudentDto {
    private Integer id;
    private String name;
    private String email;
    private String phoneNumber;
}
